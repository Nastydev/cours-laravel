<?php

use App\Http\Controllers\CategoryController as CategoryControllerAlias;
use App\Http\Controllers\PostController as PostControllerAlias;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Categories
Route::get('/categories', [CategoryControllerAlias::class, 'index'])->name('categories.index');
Route::get('/categories/create', [CategoryControllerAlias::class, 'create'])->name('categories.create');
Route::post('/categories', [CategoryControllerAlias::class, 'save'])->name('categories.save');
Route::get('/categories/{category}/edit', [CategoryControllerAlias::class, 'edit'])->name('categories.edit');
Route::put('/categories/{category}/update', [CategoryControllerAlias::class, 'update'])->name('categories.update');
Route::get('/categories/{category}', [CategoryControllerAlias::class, 'show'])->name('categories.show');
Route::delete('/categories/{category}', [CategoryControllerAlias::class, 'delete'])->name('categories.delete');

//Posts
Route::get('/posts', [PostControllerAlias::class, 'index'])->name('posts.index');
Route::get('/posts/create', [PostControllerAlias::class, 'create'])->name('posts.create');
Route::post('/posts', [PostControllerAlias::class, 'save'])->name('posts.save');
Route::get('/posts/{posts}/edit', [PostControllerAlias::class, 'edit'])->name('posts.edit');
Route::put('/posts/{posts}/update', [PostControllerAlias::class, 'update'])->name('posts.update');
//Route::delete('/posts/{posts}', [PostControllerAlias::class, 'delete'])->name('posts.delete');
