<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SavePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:posts|max:255',
            'content' => 'required|min:20',
            'img_url' => 'required',
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Le titre ne peux pas être vide',
            'title.max' => 'Le titre ne peux pas dépasser 255 caractères',
            'content.required' => 'La description ne peux pas être vide',
            'content.20' => 'La description dois contenir au moins 20 caractères',
            'img_url.required' => 'Le slug ne peux pas être vide',
            'category_id.required' => 'La catégorie ne peux pas être vide',
        ];
    }
}
