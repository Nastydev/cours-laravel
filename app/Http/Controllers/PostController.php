<?php

namespace App\Http\Controllers;

use App\Http\Requests\SavePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::with('category:id,title')
            ->select('id', 'title', 'content', 'slug', 'category_id')
            ->get();

        return view('posts.index', ['posts' => $posts]);
    }

    public function create()
    {
        $category = Category::pluck('title', 'id');

        return view('posts.create', ['categories' => $category]);
    }

    public function save(SavePostRequest $request)
    {
        echo Post::create($request->all());

    }

    public function edit($id)
    {
        $category = Category::pluck('title', 'id');

        $tags = Tag::pluck('title', 'id');

        $posts = Post::findOrFail($id);
        return view('posts.edit', [
            'posts' => $posts,
            'categories' => $category,
            'tags' => $tags,
            ]);
    }

    public function update(SavePostRequest $request, $id)
    {

        Post::find($id)->update($request->all());

        return redirect()->route('posts.index');
    }

//    public function delete(Post $post)
//    {
//
//        if ($post->delete()) {
//            return redirect()->route('post.index');
//        }
//    }
}
