<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveCategoryRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function save(SaveCategoryRequest $request)
    {
        Category::create($request->all());
        return redirect()->route('categories.index');
    }

    public function delete(Category $category)
    {

        if ($category->delete()) {
            return redirect()->route('categories.index');
        }
    }

    public function show(Category $category)
    {

        $posts = Post::select('id', 'title', 'content', 'slug', 'category_id')
            ->where('category_id', $category->id)
            ->get();

        return view('categories.show', ['posts' => $posts]);
    }

    public function edit($id)
    {

        $categroy = Category::findOrFail($id);
        return view('categories.edit', ['category'=> $categroy]);
    }

    public function update(SaveCategoryRequest $request, $id)
    {

       Category::find($id)->update($request->all());

        return redirect()->route('categories.index');
    }
}
