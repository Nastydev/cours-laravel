<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        body {
            background-color: #0d0d0d;
            color: #edf2f7;
        }
    </style>
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>
                New post
            </h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-12 justify-content-center">
            {!! Form::model($posts, ['method' => 'PUT', 'url' => '/posts/' . $posts->id . '/update']) !!}

            <div class="form-group mt-3">
                <label for="title">Title</label>
                {{Form::text('title', null, ['class' => 'form-control'])}}
            </div>
            <div class="form-group mt-3">
                <label for="content">Content</label>
                {{Form::textarea('content', null, ['class' => 'form-control'])}}
            </div>
            <div class="form-group mt-3">
                <label for="slug">Slug</label>
                {{Form::text('slug', null, ['class' => 'form-control'])}}
            </div>
            <div class="form-group mt-3">
                <label for="img_url">Img URL</label>
                {{Form::text('img_url', null, ['class' => 'form-control'])}}
            </div>
            <div class="form-group mt-3">
                <label for="img_url">categorie</label>
            {{ Form::select('category_id', $categories, ['placeholder' => '-- Choice a category --']) }}
            </div>

            <div class="form-group mt-3">
                <label for="img_url">tags</label>
                {{ Form::select('tag_id', $tags, ['multiple'=>'multiple', 'placeholder' => '-- Choice a category --']) }}
            </div>

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
            {!! Form::close() !!}
        </div>
        <div class="col-12 mt-3">
            <a href="">Back to list</a>
        </div>
    </div>
</div>

</body>
</html>
