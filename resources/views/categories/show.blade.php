<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        body {
            background-color: #0d0d0d;
            color: #edf2f7;
        }

        td {

            color: #edf2f7;

        }
    </style>
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="h1 text-center my-5">
                @foreach($posts as $post)
                Category {{ $post->category->title }}
                @endforeach
            </h1>
        </div>
        <div class="row">
            <div class="col-12 justify-content-center mt-5">
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <td>Id</td>
                        <td>Title</td>
                        <td>Content</td>
                        <td>Slug</td>
                        <td>Category</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->content }}</td>
                            <td>{{ $post->slug }}</td>
                            <td>{{ $post->category->title }}</td>
                            <td><a class="btn btn-success" href="#">edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>
